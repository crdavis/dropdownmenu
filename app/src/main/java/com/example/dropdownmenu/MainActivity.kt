package com.example.dropdownmenu

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.outlined.Email
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.example.dropdownmenu.ui.theme.DropDownMenuTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DropDownMenuTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        ShowScreen()
                    }
                }
            }
        }
    }
}

@Composable
fun ShowScreen() {
    //State of the menu
    var expanded by remember { mutableStateOf(false) }
    val contextForToast = LocalContext.current.applicationContext

    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(align = Alignment.Center),
        contentAlignment = Alignment.Center
    ) {
        //Vertical 3 dots icon
        IconButton(onClick = { expanded = true }) {
            Icon(Icons.Default.MoreVert, contentDescription = "Open Menu")
        }

        //Menu
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {

            // Menu item
            DropdownMenuItem(
                text = { Text("Mobile Dev") },
                onClick = {
                    Toast.makeText(contextForToast, "Mobile Dev", Toast.LENGTH_SHORT).show()
                    expanded = false
                }
            )

            //Menu item
            DropdownMenuItem(
                text = { Text("Web Dev") },
                onClick = {
                    Toast.makeText(contextForToast, "Web Dev", Toast.LENGTH_SHORT).show()
                    expanded = false
                }
            )

            //Menu item
            DropdownMenuItem(
                text = { Text("Programming V") },
                onClick = {
                    Toast.makeText(contextForToast, "Programming V", Toast.LENGTH_SHORT).show()
                    expanded = false
                }
            )

            //Menu item
            DropdownMenuItem(
                text = { Text("Data Com") },
                onClick = {
                    Toast.makeText(contextForToast, "Data Com", Toast.LENGTH_SHORT).show()
                    expanded = false
                },

                )

            //Menu item
            DropdownMenuItem(
                text = { Text("Contact us") },
                onClick = {
                    Toast.makeText(contextForToast, "Contact us", Toast.LENGTH_SHORT).show()
                    expanded = false
                },
                leadingIcon = {
                    Icon(Icons.Outlined.Email, contentDescription = null)
                }
            )
        }
    }
}


@Preview(showBackground = true)
@Composable
fun ShowScreenPreview() {
    DropDownMenuTheme {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            ShowScreen()
        }
    }
}